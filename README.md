# GameTTS for Jaco

Alternative german voices for [Jaco](https://gitlab.com/Jaco-Assistant/Jaco-Master). \
Built upon: https://github.com/lexkoro/GameTTS

[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

<br/>

## Setup

If you have a nvidia gpu you may want to install the gpu tools:
[link](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker)

Build container:

```bash
git clone https://gitlab.com/DANBER/Jaco-GameTTS
git clone https://github.com/lexkoro/GameTTS  extern/GameTTS/
docker build -f Containerfile -t game-tts .
```

Copy `module_topic_keys.json` into `userdata` directory

Run the main script:

```bash
docker run --network host --rm --runtime=nvidia --privileged \
    --volume `pwd`/extern/GameTTS/:/GameTTS/ \
    --volume `pwd`/moduldata/cache/:/cache/ \
    --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
    --volume `pwd`/action_tts.py:/Jaco-Master/text-to-speech/action_tts.py:ro \
    --volume `pwd`/synthesize.py:/Jaco-Master/text-to-speech/synthesize.py:ro \
    -it game-tts python3 /Jaco-Master/text-to-speech/action_tts.py
```

<br/>

To replace the default text-to-speech module of Jaco, delete the `text_to_speech` service from `Jaco-Master/runfiles/start-all-compose.yml`. \
You can replace it with the contents of the `docker-compose.yml` here (can't use the gpu) or start the container extra with the command above.

<br/>

## Debugging

Connect to container:

```bash
docker run --privileged --network host --ipc host --rm --device /dev/snd \
    --gpus all --shm-size=1g --ulimit memlock=-1 --ulimit stack=67108864 \
    --volume `pwd`/extern/GameTTS/:/GameTTS/ \
    --volume `pwd`/moduldata/cache/:/cache/ \
    --volume `pwd`/userdata/:/Jaco-Master/userdata/:ro \
    --volume `pwd`/action_tts.py:/Jaco-Master/text-to-speech/action_tts.py:ro \
    --volume `pwd`/generate_cache.py:/Jaco-Master/text-to-speech/generate_cache.py:ro \
    --volume `pwd`/synthesize.py:/Jaco-Master/text-to-speech/synthesize.py:ro \
    --volume ~/.asoundrc:/etc/asound.conf:ro \
    --volume /tmp/.X11-unix:/tmp/.X11-unix \
    --env="DISPLAY" --env="QT_X11_NO_MITSHM=1" \
    -it game-tts
```

Test synthesizing with gui:

```bash
cd /GameTTS/GameTTS/
python3 main.py

# Go to
http://localhost:8080
```

Test synthesizing with script:

```bash
python3 /Jaco-Master/text-to-speech/synthesize.py \
    --savepath /cache/tmp/test123.wav \
    --speakerid 38 --speed 1.0 --va 0.5 --vb 0.7 \
    --text "Wollen wir eine Runde Karten spielen?"

aplay /cache/tmp/test123.wav
```

Generate audio cache from a file:
  - Create a list of sentences at `userdata/my_texts.json`
  - Call `python3 /Jaco-Master/text-to-speech/action_tts.py`
